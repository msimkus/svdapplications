#install.packages("jpeg")
#install.packages("png")
library(jpeg)
library(png)

par(pty="s")

#####   Vaizdu glaudinimas   #####

# paveikslelio spausdinimas nespalvotai
plot_img <- function(img){
  image(t(img)[,nrow(img):1], col=gray((0:255)/255), axes=FALSE)
}

img <- readJPEG("***.jpg")

# spalvoto paveikslelio konvertavimas i nespalvota, remiantis sviesos ryskumo (angl. luminosity) metodu:
# Black&White = 0.21*Red + 0.72*Green + 0.07*Blue
bw_img <- 0.21*img[,,1] + 0.72*img[,,2] + 0.07*img[,,3]

plot_img(bw_img)

# paveikslelio glaudinimas, remiantis SVD metodu
svd <- svd(bw_img)

# tikriniu reiksmiu, nelygiu 0, skaicius
s <- 0
for (i in 1:length(svd$d)){
  if (round(svd$d[i]) > 0){
    s <- s + 1
  }
}
s

k <- 50 # tikriniu reiksmiu skaicius
compressed_img <- svd$u[,1:k] %*% diag(svd$d[1:k]) %*% t(svd$v[,1:k])
plot_img(compressed_img)
mean(bw_img - compressed_img) # paklaida

#####   Triuksmo sumazinimas   #####

noisy_img <- readPNG("***.png")[,,1]

svd_noisy <- svd(noisy_img)

threshold <- 20 # slenkstis, uz kuri mazesnes tikrines reiksmes prilyginamos 0
for(i in 1:length(svd_noisy$d)){
  if (svd_noisy$d[i] < threshold){
    svd_noisy$d[i] <- 0
  }
}

denoised_img <- svd_noisy$u %*% diag(svd_noisy$d) %*% t(svd_noisy$v)
plot_img(denoised_img)

#####   Vaizdu atpazinimas   #####

# apmokymo ir testavimo paveiksleliai-veidai
n <- 6 # apmokymo imties dydis
train_1 <- readJPEG("***.jpg")
train_2 <- readJPEG("***.jpg")
train_3 <- readJPEG("***.jpg")
train_4 <- readJPEG("***.jpg")
train_5 <- readJPEG("***.jpg")
train_6 <- readJPEG("***.jpg")
test <- readJPEG("***.jpg")

S <- cbind(as.vector(train_1), as.vector(train_2), as.vector(train_3), as.vector(train_4), as.vector(train_5), as.vector(train_6))
#plot_img(matrix(as.vector(S[,1]), nrow = 150, ncol = 150))

train_avg <- (S[,1] + S[,2] + S[,3] + S[,4] + S[,5] + S[,6]) / n # "vidutinis" veidas
plot_img(matrix(train_avg, nrow = 150, ncol = 150))

A <- S
for(i in 1:n){
  A[,i] <- A[,i] - train_avg
}

svd_A <- svd(A)

x_train_1 <- t(svd_A$u) %*% as.matrix(S[,1] - train_avg)
x_train_2 <- t(svd_A$u) %*% as.matrix(S[,2] - train_avg)
x_train_3 <- t(svd_A$u) %*% as.matrix(S[,3] - train_avg)
x_train_4 <- t(svd_A$u) %*% as.matrix(S[,4] - train_avg)
x_train_5 <- t(svd_A$u) %*% as.matrix(S[,5] - train_avg)
x_train_6 <- t(svd_A$u) %*% as.matrix(S[,6] - train_avg)

x_test <- t(svd_A$u) %*% (as.vector(test) - train_avg)

# testavimo paveikslelio atstumai iki apmokymo paveiksleliu
eps1 <- sqrt(t(x_test - x_train_1) %*% (x_test - x_train_1))
eps2 <- sqrt(t(x_test - x_train_2) %*% (x_test - x_train_2))
eps3 <- sqrt(t(x_test - x_train_3) %*% (x_test - x_train_3))
eps4 <- sqrt(t(x_test - x_train_4) %*% (x_test - x_train_4))
eps5 <- sqrt(t(x_test - x_train_5) %*% (x_test - x_train_5))
eps6 <- sqrt(t(x_test - x_train_6) %*% (x_test - x_train_6))
eps1
eps2
eps3
eps4
eps5
eps6